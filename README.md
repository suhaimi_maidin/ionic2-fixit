## FixIT 

It is a sample reporting application. 

```bash
$ npm install
$ ionic serve
```

### troubleshooting

In case you are not able to run it (outdated), run

```bash
$ npm update
$ ionic serve
```
## Screens

![picture](screens/fixit.png)

![picture](screens/about.png)

![picture](screens/add_report.png)

![picture](screens/qr_scanner.png)

![picture](screens/camera.png)

![picture](screens/report.png)

### Sample QR Code Tag

![picture](screens/qrcode.png)

