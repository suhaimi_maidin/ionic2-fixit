import { Report } from './../../models/report';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataProvider {

  public reports: Report[] = new Array();
  
  constructor(public http: Http) {
  }

  setReport(report: Report) {
    this.reports = this.reports || [];
    report.id = new Date().toISOString()
    this.reports.push(report);
    localStorage.setItem('MyReports', JSON.stringify(this.reports));
  }

  getReports() {
    this.reports = JSON.parse(localStorage.getItem('MyReports')) as Report[];
    return this.reports;
  }

  removeReport(report) {
    this.reports = this.reports.filter(element => element.id !== report.id)
    localStorage.setItem('MyReports', JSON.stringify(this.reports));
    return this.reports;
  }

  editReport(oldReport: Report, newReport: Report) {
    newReport.modified = new Date().toUTCString();
    // remove old report
    this.removeReport(oldReport)
    // save new report
    this.setReport(newReport)
  }
}
