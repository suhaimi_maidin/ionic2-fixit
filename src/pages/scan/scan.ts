import { PhotoPage } from './../photo/photo';
import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@IonicPage()
@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html',
})
export class ScanPage {

  public scannedText: string;
  public startText: string;
  public endText: string;
  public startButton: boolean;
  public endButton: boolean;
  public location: string = '';
  public token: string = 'test';
  public user_id: string;
  public data: any;

  constructor(
    private navCtrl: NavController,
    public alertCtrl: AlertController,
    private barcodeScanner: BarcodeScanner
  ) {
  }

  public scanQR() {
    // if (!this.token) {
    //   this.navCtrl.push(LoginPage);
    // }
    this.startButton = false;
    this.endButton = false;

    this.barcodeScanner.scan().then((barcodeData) => {
      if (barcodeData.cancelled) {
        console.log("User cancelled the action!");
        this.startButton = false;
        this.endButton = false;
        return false;
      }
      console.log("Scanned successfully!");
      this.scannedText = barcodeData.text;
    }, (err) => {
      console.log(err);
    });
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Alert!',
      subTitle: 'Please configure your application Settings!',
      buttons: ['OK']
    });
    alert.present();
  }

  getLocation() {
    let status: boolean = true;
    if (!this.location) {
      status = true;
    } else {
      status = false;
    }
    return status;
  }

  public capturePhoto() {
    this.navCtrl.push(PhotoPage);
  }

}
