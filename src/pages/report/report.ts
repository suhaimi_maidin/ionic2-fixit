import { EditReportPage } from './../edit-report/edit-report';
import { AddReportPage } from './../add-report/add-report';
import { DataProvider } from './../../providers/data/data';
import { Report } from './../../models/report';
import { Component } from '@angular/core';
import { NavController, AlertController, ActionSheetController  } from 'ionic-angular';


@Component({
  selector: 'page-report',
  templateUrl: 'report.html'
})
export class ReportPage {

  public report: Report = new Report();
  public reports: Report[];
  public isReportsAvailable: boolean = false;

  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    private dataProvider: DataProvider,
    public actionSheetCtrl: ActionSheetController
  ) {
    this.reports = this.dataProvider.getReports();
    if (this.reports) {
      this.isReportsAvailable = true;
    }  else {
      this.isReportsAvailable = false;
    }
  }

  ionViewWillEnter(){
    this.reports = this.dataProvider.getReports();    
    if (this.reports) {
      this.isReportsAvailable = true;
    } else {
      this.isReportsAvailable = false;
    }
  }

  doRefresh(refresher) {
    setTimeout(() => {
      console.log('Async operation has ended');
      this.reports = this.dataProvider.getReports();
      refresher.complete();
    }, 2000);
  }

  public addReport() {
    this.navCtrl.push(AddReportPage)
  }

  public editReport(currentReport) {
    this.navCtrl.push(EditReportPage, {oldReport: currentReport })
  }

  remove(myTask) {
    this.reports = this.dataProvider.removeReport(myTask);    
  }

  public removeConfirm(report) {
    let alert = this.alertCtrl.create({
      title: 'Confirm Remove',
      message: 'Do you want to delete this report? (' + report.title + ')',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            this.remove(report);
          }
        }
      ]
    });
    alert.present();
  }

  public assignReport(report) {
    let alert = this.alertCtrl.create({
      title: 'Future Implementation!',
      message: 'To be implemented later ...  (' + report.title + ')',
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  reportActionSheet(report) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Actions ',
      buttons: [
        {
          text: 'Assign',
          role: 'assign',
          handler: () => {
            console.log('Destructive clicked');
            this.assignReport(report);
          }
        },
        {
          text: 'Edit',
          role: 'edit',
          handler: () => {
            console.log('Destructive clicked');
            this.editReport(report);
          }
        },
        {
          text: 'Remove',
          role: 'remove',
          handler: () => {
            this.removeConfirm(report)
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
