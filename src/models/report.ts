export class Report {        
    id: string
    title : string;
    description: string;
    created : string;
    modified : string;
    due: string;
    assignTo: string
    createdBy: string
    assetNumber: string
    photo: string
}