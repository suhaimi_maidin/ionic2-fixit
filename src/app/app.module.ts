import { ReportPage } from './../pages/report/report';
import { EditReportPage } from './../pages/edit-report/edit-report';
import { AddReportPage } from './../pages/add-report/add-report';
import { SortByPipe } from './../pipes/sort-by/sort-by';
import { PhotoPage } from './../pages/photo/photo';
import { ProfilePage } from './../pages/profile/profile';
import { ScanPage } from './../pages/scan/scan';
import { LoginPage } from './../pages/login/login';
import { AboutPage } from './../pages/about/about';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { HttpModule } from '@angular/http';

import { ApiProvider } from '../providers/api/api';
import { DataProvider } from '../providers/data/data';

@NgModule({
  declarations: [
    MyApp,
    ReportPage,
    AboutPage,
    PhotoPage,
    LoginPage,
    ScanPage,
    ProfilePage,
    AddReportPage,
    EditReportPage,
    SortByPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ReportPage,
    AboutPage,
    PhotoPage,
    LoginPage,
    ScanPage,
    ProfilePage,
    AddReportPage,
    EditReportPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    File,
    Transfer,
    Camera,
    FilePath,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiProvider,
    DataProvider
  ]
})
export class AppModule { }
